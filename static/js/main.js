// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


$(document).ready(function(){
    /* The following code is executed once the DOM is loaded */

    $(".todoList").sortable({
        axis        : 'y',              // Only vertical movements allowed
        containment : 'window',         // Constrained by the window
        update      : function(){       // The function is called after the todos are rearranged

            // The toArray method returns an array with the ids of the todos
            var arr = $(".todoList").sortable('toArray');

            // Striping the todo- prefix of the ids:

            arr = $.map(arr,function(val,key){
                return val.replace('todo-','');
            });
            $.post('/rearrange-todo',{positions:arr});

            // Saving with AJAX
            // $.ajax('/rearrange-todo',
            //     {
            //         data: {
            //             positions: arr,
            //         },
                     
            //         // whether this is a POST or GET request
            //         type: "GET",
            //     }
            // );
        }
    });

    // A global variable, holding a jQuery object
    // containing the current todo item:

    var currentTODO;


// When a double click occurs, just simulate a click on the edit button:
    $('.todo').on('dblclick',function(){
        $(this).find('a.edit').click();
    });

    // If any link in the todo is clicked, assign
    // the todo item to the currentTODO variable for later use.

    $('.todo a').on('click',function(e){

        currentTODO = $(this).closest('.todo');
        currentTODO.data('id',currentTODO.attr('id').replace('todo-',''));

        e.preventDefault();
    });

    // Listening for a click on a delete button:

    $('.todo a.done').on('click',function(e){
        e.preventDefault();

        $.ajax('/done-todo',
                {
                    data: {
                        todo_id: currentTODO.data('id'),
                    },
                     
                    // whether this is a POST or GET request
                    type: "POST",
                    statusCode: {
                        200: function (data, textStatus, jqXHR){
                            currentTODO.fadeOut('slow');

                        },
                        404: function(jqXHR, textStatus, errorThrown){
                        }
                    }
                }
            );
    });

    // Listening for a click on a edit button

    $('.todo a.edit').on('click',function(){

        var container = currentTODO.find('.text');

        if(!currentTODO.data('origText'))
        {
            // Saving the current value of the ToDo so we can
            // restore it later if the user discards the changes:

            currentTODO.data('origText',container.text());
        }
        else
        {
            // This will block the edit button if the edit box is already open:
            return false;
        }

        $('<input type="text">').val(container.text()).appendTo(container.empty());

        // Appending the save and cancel links:
        container.append(
            '<div class="editTodo">'+
                '<a href="" class="saveChanges" >Spremi</a> ili <a href="" class="discardChanges" >Odustani</a>'+
            '</div>'
        );

        $('.todo a.discardChanges').on('click', function(e){
            e.preventDefault();
            currentTODO.find('.text')
                        .text(currentTODO.data('origText'))
                        .end()
                        .removeData('origText');
        });

        // The save changes link:

        $('.todo a.saveChanges').on('click',function(e){
            e.preventDefault();
            var text = currentTODO.find("input[type=text]").val();

            $.ajax('/edit-todo',
                {
                    data: {
                        todo_id: currentTODO.data('id'),
                        todo_str: text
                    },
                     
                    // whether this is a POST or GET request
                    type: "POST",
                    statusCode: {
                        200: function (data, textStatus, jqXHR){
                            currentTODO.removeData('origText')
                                .find(".text")
                                .text(text);

                        },
                        404: function(jqXHR, textStatus, errorThrown){
                            $(this).find('.todo a.discardChanges').click();
                        }
                    }
                }
            );

            
        });

    });

    // The cancel edit link:
    

    

    // The Add New ToDo button:

    $('.add-todo').click(function(e){
        alertify.set({ buttonReverse: true,
        labels: {
            ok     : "Spremi",
            cancel : "Odustani"
        } });
        alertify.prompt("Nova TODO stavka", function (e, str) {
            // str is the input text
            if (e) {
                $.post("/new-todo",{'todo_str':str},function(msg){
                    // Appending the new todo and fading it into view:
                    $('.todoList').prepend(msg).fadeIn('');

                });
                var arr = $(".todoList").sortable('toArray');

                    // Striping the todo- prefix of the ids:

                    arr = $.map(arr,function(val,key){
                        return val.replace('todo-','');
                    });
                    $.post('/rearrange-todo',{positions:arr});
            } else {
                // user clicked "cancel"
            }
        }, "Nova stavka");

        e.preventDefault();
    });

    $('.note_box a.delete').on('click',function(e){
        e.preventDefault();
        var clicked = $(this);

        alertify.set({ buttonReverse: true,
            labels: {
                ok     : "Da",
                cancel : "Ne"
        } });
        
        alertify.confirm("Jeste li sigurni da želite obrisati bilješku?", function (e) {
            if (e) {
                $.post("/delete-note",{'note_id':clicked.data('id')},function(msg){
                    location.reload();
                });
            } else {
                // user clicked "cancel"
            }
        });

        
    });

    $('a.delete-activity').on('click',function(e){
        e.preventDefault();
        var clicked = $(this);

        alertify.set({ buttonReverse: true,
            labels: {
                ok     : "Da",
                cancel : "Ne"
        } });
        
        alertify.confirm("Jeste li sigurni da želite obrisati aktivnost?", function (e) {
            if (e) {
                $.post("/delete-activity",{'activity_id':clicked.data('id')},function(msg){
                     window.location = '/kalendar-aktivnosti';
                });
            } else {
                // user clicked "cancel"
            }
        });

        
    });

    $('.update-points').click(function(e){
        e.preventDefault();
        var clicked = $(this);
        alertify.set({ buttonReverse: true,
        labels: {
            ok     : "Spremi",
            cancel : "Odustani"
        } });
        alertify.prompt("Unesi bodove", function (e, str) {
            // str is the input text
            if (e) {
                $.post("/update-points",{'activity_id':clicked.data('id'), 'points':str},function(msg){
                    // Appending the new todo and fading it into view:
                    location.reload()

                });
            } else {
                // user clicked "cancel"
            }
        }, "0.00");

        e.preventDefault();
    });

    $('.post_box a.delete').on('click',function(e){
        e.preventDefault();
        var clicked = $(this);

        alertify.set({ buttonReverse: true,
            labels: {
                ok     : "Da",
                cancel : "Ne"
        } });
        
        alertify.confirm("Jeste li sigurni da želite obrisati objavu?", function (e) {
            if (e) {
                $.post("/delete-post",{'post_id':clicked.data('id')},function(msg){
                    location.reload();
                });
            } else {
                // user clicked "cancel"
            }
        });

        
    });

    $('.delete_file').on('click',function(e){
        e.preventDefault();
        var clicked = $(this);

        alertify.set({ buttonReverse: true,
            labels: {
                ok     : "Da",
                cancel : "Ne"
        } });
        
        alertify.confirm("Jeste li sigurni da želite obrisati datoteku iz ove objave?", function (e) {
            if (e) {
                $.post("/delete-file",{'file_id':clicked.data('id')},function(msg){
                    location.reload();
                });
            } else {
                // user clicked "cancel"
            }
        });

        
    });

}); // Closing $(document).ready()