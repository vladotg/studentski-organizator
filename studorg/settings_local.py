# -*- coding: utf-8 -*-

from settings import *
import sys

DEBUG = True

ALLOWED_HOSTS = ['edukacije.24sata.hr', 'localhost']

JOHNNY_CACHE = False
DEBUG_TOOLBAR = False
TASTYPIE_FULL_DEBUG = False
THUMBNAIL_DEBUG = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'studorgdb',                      # Or path to database file if using sqlite3.
        'USER': 'studorgdbuser',                      # Not used with sqlite3.
        'PASSWORD': 'studorgdbuser',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}


if DEBUG_TOOLBAR:
    INSTALLED_APPS += (
        'debug_toolbar',
    )   

    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )   

    INTERNAL_IPS = ('127.0.0.1',)

SENTRY = False

POSTNOT = False