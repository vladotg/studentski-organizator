# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.conf import settings
from university.models import *
from user_zone.models import *
from django.contrib.contenttypes.models import ContentType
from django.db.models import Max

import requests
import time

localtime = time.localtime()

class Command(BaseCommand):
    """
    Management commands for syncing with ISVU app.
    """
    def handle(self, *args, **options):
        self.sync_places()
        self.sync_universities()
        self.sync_colleges()
        self.sync_courses()
        self.sync_employees()
        self.sync_course_employees()
        self.sync_students()
        self.sync_student_courses()
        self.sync_deleted_items()

    def sync_places(self):
        last = Place.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'place?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                try:
                    place = Place(**obj)
                    place.save()
                except:
                    pass
        except KeyError:
            pass

    def sync_universities(self):
        last = University.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'university?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                university = University(**obj)
                university.save()
        except KeyError:
            pass

    def sync_colleges(self):
        last = College.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'college?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                place = Place.objects.get(id=obj['place']['id'])
                university = University.objects.get(id=obj['university']['id'])
                del obj['place']
                del obj['university']
                college = College(place=place, university=university, **obj)
                college.save()
        except KeyError:
            pass

    def sync_courses(self):
        last = Course.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'course?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                college = College.objects.get(id=obj['college']['id'])
                del obj['college']
                course = Course(college=college, **obj)
                course.save()
        except KeyError:
            pass

    def sync_employees(self):
        last = Employee.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'employee?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                employee = Employee(**obj)
                employee.save()
        except KeyError:
            pass

    def sync_course_employees(self):
        last = CourseEmployee.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'course_employee?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                course = Course.objects.get(id=obj['course']['id'])
                employee = Employee.objects.get(id=obj['employee']['id'])
                del obj['course']
                del obj['employee']
                course_employee = CourseEmployee(course=course, employee=employee, **obj)
                course_employee.save()
        except KeyError:
            pass

    def sync_students(self):
        last = Student.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'student?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                if obj['college'] is not None:
                    college = College.objects.get(id=obj['college']['id'])
                else:
                    college = None
                del obj['college']
                student = Student(college=college, **obj)
                student.save()
        except KeyError:
            pass

    def sync_student_courses(self):
        last = StudentCourse.objects.all().aggregate(Max('last_update'))
        if last['last_update__max'] is not None:
            converted_date = last['last_update__max'] + timedelta( seconds=1 )
            date = converted_date.strftime("%Y-%m-%d %H:%M:%S")  
        else:
            date = datetime(1950, 1, 1, 0, 0).strftime("%Y-%m-%d %H:%M")

        r = requests.get(settings.ISVU_URL + 'student_course?last_update__gt=%s' % date, auth=('vrosancic', 'admin'))        
        json = r.json()

        try:
            for obj in json['objects']:
                del obj['resource_uri']
                course = Course.objects.get(id=obj['course']['id'])
                student = Student.objects.get(id=obj['student']['id'])
                del obj['course']
                del obj['student']
                student_course = StudentCourse(course=course, student=student, **obj)
                student_course.save()
        except KeyError:
            pass
        

    def sync_deleted_items(self):
        r = requests.get(settings.ISVU_URL + 'deleted_item', auth=('vrosancic', 'admin'))       
        json = r.json()

        try:
            for obj in json['objects']:
                try:
                    content_type = ContentType.objects.get(app_label=obj['app'], model=obj['model'])
                    item = content_type.get_object_for_this_type(id=obj['object_id'])
                    item.delete()
                    requests.delete(settings.ISVU_URL + 'deleted_item/%s' % obj['id'], auth=('vrosancic', 'admin'))
                except:
                    continue
        except KeyError:
            pass
