# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from decimal import *
from django.db import transaction

from models import Employee, CourseEmployee
from organizer.models import Literature, Activity, Post, PostComment, File
from user_zone.models import StudentCourse

from forms import *


class CourseView(TemplateView):
    template_name = "university/course.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CourseView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CourseView, self).get_context_data(**kwargs)

        student_course_id = kwargs.get('course_id')
   
        student_course = get_object_or_404(StudentCourse, id=student_course_id)
        student_courses = StudentCourse.objects.filter(student=self.request.user)

        literature = Literature.objects.filter(course = student_course)

        course_employees = CourseEmployee.objects.filter(course=student_course.course)

        activities = Activity.objects.filter(course=student_course, student=self.request.user)\
            .order_by('start')

        context['top_nav'] = [{'label':'Zid s objavama', 'url':reverse('course_wall', \
            args=[student_course.course.slug, student_course.id]), 'icon':'icon-comment'}]
        context['pagetitle'] = 'Moji kolegiji'
        context['student_courses'] = student_courses
        context['student_course'] = student_course
        context['literature'] = literature
        context['course_employees'] = course_employees
        context['activities'] = activities

        return context

class CourseWallView(TemplateView):
    template_name = "university/course_wall.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CourseWallView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CourseWallView, self).get_context_data(**kwargs)

        student_course_id = kwargs.get('course_id')
   
        student_course = get_object_or_404(StudentCourse, id=student_course_id)
        posts_list = Post.objects.filter(course = student_course)

        paginator = Paginator(posts_list, 4)
    
        page = self.request.GET.get('page')
        
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)

        student_courses = StudentCourse.objects.filter(student=self.request.user)

        context['top_nav'] = [{'label':'Nova objava', 'url':reverse('new_post', \
            args=[student_course.course.slug, student_course.id]), 'icon':'icon-plus'}]
        context['student_course'] = student_course
        context['posts'] = posts
        context['pagetitle'] = 'Moji kolegiji'
        context['student_courses'] = student_courses

        return context

class EditPostView(TemplateView):
    template_name = "university/edit_post.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        post_id = kwargs.get('post_id', None)
        course_id = kwargs.get('course_id', None)
        self.course = get_object_or_404(StudentCourse, id=course_id) 
        try:
            self.course_post = Post.objects.get(id=post_id, course=self.course)
            if self.course_post.student != self.request.user:
                return HttpResponse('Unauthorized', status=401)
        except Post.DoesNotExist:
            self.course_post = Post(student=self.request.user, course=self.course)
        return super(EditPostView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditPostView, self).get_context_data(**kwargs)

        form = PostForm(instance=self.course_post)
        files = File.objects.filter(post = self.course_post)

        context['files'] = files
        context['form'] = form
        context['post'] = self.course_post
        context['pagetitle'] = 'Moji kolegiji'

        return context

    @transaction.commit_on_success
    def post(self, *args, **kwargs):
        form = PostForm(self.request.POST, instance=self.course_post)

        if form.is_valid():
            post = form.save(commit=False)
            post.course = self.course
            post.student = self.course.student
            post.save()

            for post_file in self.request.FILES.getlist('files'):
                new_file = File(student=self.request.user, post=post, name=post_file.name, date=datetime.now(), filename=post_file)
                new_file.save()

            messages.success(self.request, u"Uspješno ste spremili objavu!")
            return HttpResponseRedirect(reverse('course_wall', args=[self.course.course.slug, self.course.id]))

        return self.render_to_response({'form': form, 'pagetitle':'Moji kolegiji'})

class PostCommentsView(TemplateView):
    template_name = "university/comments.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PostCommentsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostCommentsView, self).get_context_data(**kwargs)

        post_id = self.request.GET.get('post_id')
   
        course_post = get_object_or_404(Post, id=post_id)
        comments = PostComment.objects.filter(post = course_post)

        context['comments'] = comments
        context['post_id'] = post_id

        return context

class PostFilesView(TemplateView):
    template_name = "university/post_files.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PostFilesView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostFilesView, self).get_context_data(**kwargs)

        post_id = self.request.GET.get('post_id')
   
        course_post = get_object_or_404(Post, id=post_id)
        files = File.objects.filter(post = course_post)

        context['files'] = files
        context['post_id'] = post_id

        return context


def add_comment(request, *args, **kwargs):
    text = request.GET.get('text')
    post_id = request.GET.get('post_id')
    post = Post.objects.get(id=post_id)
    post_comment = PostComment(student=request.user, text=text, post=post)
    post_comment.save()
    return render(request, 'university/comment.html', {'comment': post_comment})

def delete_comment(request):
    comment_id = int(request.POST.get('comment_id', None))
    try:
        comment = PostComment.objects.get(id=comment_id)
        comment.delete()
        return HttpResponse('OK', status=200)
    except PostComment.DoesNotExist:
        return HttpResponse("Exception", status=404)

def delete_post(request):
    post_id = int(request.POST.get('post_id', None))
    try:
        post = Post.objects.get(id=post_id)
        post.delete()
        return HttpResponse('OK', status=200)
    except Post.DoesNotExist:
        return HttpResponse("Exception", status=404)

def delete_file(request):
    file_id = int(request.POST.get('file_id', None))
    try:
        post_file = File.objects.get(id=file_id)
        post_file.delete()
        return HttpResponse('OK', status=200)
    except File.DoesNotExist:
        return HttpResponse("Exception", status=404)

class SendMailView(TemplateView):
    template_name = "university/send_mail.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        employee_id = kwargs.get('employee_id', None)
        self.course_slug = kwargs.get('course_slug', None)
        self.course_id = kwargs.get('course_id', None)

        self.employee = get_object_or_404(Employee, id=employee_id)
        return super(SendMailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SendMailView, self).get_context_data(**kwargs)
        
        form = SendMailForm()

        context['form'] = form
        context['employee'] = self.employee
        context['pagetitle'] = 'Moji kolegiji'

        return context

    def post(self, *args, **kwargs):
        form = SendMailForm(self.request.POST)

        if form.is_valid():
            cd = form.cleaned_data
            from django.core.mail import send_mail
            subject = cd['subject']
            body = cd['message']
            from_email = self.request.user.email
            
            try:
                send_mail(subject, body, from_email, [self.employee.email])
                messages.success(self.request, u"Uspješno ste poslali mail!")
            except:
                messages.success(self.request, u"Dogodila se greška u slanju maila!")
            
            return HttpResponseRedirect(reverse('course', args=[self.course_slug, self.course_id]))

        return self.render_to_response({'form': form, 'employee':self.employee, 'pagetitle':'Moji kolegiji'})