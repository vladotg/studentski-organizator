# -*- coding: utf-8 -*-
from django.db import models

from django.utils.translation import ugettext as _

class Place(models.Model):
    zip_code = models.IntegerField(verbose_name=u'Poštanski broj')
    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")

    def __unicode__(self):
        return unicode(self.zip_code)

    class Meta:
        verbose_name = _(u"Mjesto")
        verbose_name_plural = _(u"Mjesta")

class University(models.Model):
    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _(u"Sveučilište")
        verbose_name_plural = _(u"Sveučilišta")

class College(models.Model):
    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    address = models.CharField(verbose_name=u'Adresa', max_length=255)
    place = models.ForeignKey(Place, verbose_name=u'Mjesto')
    university = models.ForeignKey(University, verbose_name=u'Sveučilište')
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _(u"Fakultet")
        verbose_name_plural = _(u"Fakulteti")

class Course(models.Model):
    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    slug = models.SlugField(verbose_name=_(u"Slug/URL"), max_length=255)
    college = models.ForeignKey(College, verbose_name=u'Fakultet')
    pass_threshold = models.DecimalField(verbose_name=_(u"Prag za prolaz"), default=50, max_digits = 5, decimal_places = 2)
    description = models.TextField(verbose_name=u'Opis', null=True, blank=True)
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")    

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _(u"Kolegij")
        verbose_name_plural = _(u"Kolegiji")

class Employee(models.Model):
    EMPLOYEE_TYPE = (
        ('A', _(u"Asistent")),
        ('P', _(u"Profesor")),    
    )
    title = models.CharField(verbose_name=u'Titula', max_length=40, null=True, blank=True)
    first_name = models.CharField(verbose_name=_(u"Ime"), max_length=255)
    last_name = models.CharField(verbose_name=_(u"Prezime"), max_length=255)
    email = models.EmailField(verbose_name='E-mail', max_length=255, unique=True)
    employee_type = models.CharField(verbose_name=_(u"Tip zaposlenika"), max_length=1, null=True, choices=EMPLOYEE_TYPE)
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")

    def __unicode__(self):
        return unicode("%s %s" % (self.first_name, self.last_name))

    class Meta:
        verbose_name = _(u"Izvođač")
        verbose_name_plural = _(u"Izvođači")


class CourseEmployee(models.Model):
    course = models.ForeignKey(Course, verbose_name=u'Kolegij')
    employee = models.ForeignKey(Employee, verbose_name=u'Izvođač')
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")

    def __unicode__(self):
        return unicode("%s %s" % (self.course.name, self.employee.last_name))

    class Meta:
        verbose_name = _(u"Izvođač kolegija")
        verbose_name_plural = _(u"Izvođači kolegija")      