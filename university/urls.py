# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django.views.generic import RedirectView, TemplateView

from views import *


urlpatterns = patterns('',  
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)$', CourseView.as_view(), name="course"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/posalji-mail/(?P<employee_id>\d+)$', SendMailView.as_view(), name="send_mail"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/objave$', CourseWallView.as_view(), name="course_wall"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/nova-objava$', EditPostView.as_view(), name="new_post"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/uredi-objavu/(?P<post_id>\d+)$', EditPostView.as_view(), name="edit_post"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/post-comments/$', PostCommentsView.as_view(), name="post_comments"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/add-comment/$', add_comment, name="add_comment"),
    url(r'^kolegij/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)/post-files/$', PostFilesView.as_view(), name="post_files"),
    url(r'^delete-post$', delete_post, name="delete_post"),
    url(r'^delete-file$', delete_file, name="delete_file"),
    url(r'^delete-comment$', delete_comment, name="delete_comment"),
)
