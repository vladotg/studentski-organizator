#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin 
from models import *

class PlaceAdmin(admin.ModelAdmin):
    list_display = ['name', 'zip_code', 'last_update']
    search_fields = ['name', 'zip_code']
    exclude = ['last_update']

admin.site.register(Place, PlaceAdmin)

class UniversityAdmin(admin.ModelAdmin):
    list_display = ['name', 'last_update']
    search_fields = ['name']
    exclude = ['last_update']

admin.site.register(University, UniversityAdmin)

class CollegeAdmin(admin.ModelAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    list_display = ['name', 'university', 'place', 'last_update']
    list_filter = ['university', 'place']
    search_fields = ['name']
    exclude = ['last_update']

admin.site.register(College, CollegeAdmin)

class CourseEmployeeInline(admin.TabularInline):
    model = CourseEmployee
    exclude = ['last_update']

class CourseAdmin(admin.ModelAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    list_display = ['name', 'slug', 'college', 'pass_threshold', 'last_update']
    list_filter = ['college']
    raw_id_fields = ['college']
    search_fields = ['name']
    exclude = ['last_update']
    inlines = [CourseEmployeeInline]

admin.site.register(Course, CourseAdmin)

class EmployeeAdmin(admin.ModelAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    list_display = ['last_name', 'first_name', 'title', 'email', 'employee_type', 'last_update']
    list_filter = ['employee_type']
    search_fields = ['first_name', 'last_name']
    exclude = ['last_update']

admin.site.register(Employee, EmployeeAdmin)

class CourseEmployeeAdmin(admin.ModelAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    list_display = ['employee', 'course', 'last_update']
    list_filter = ['course']
    exclude = ['last_update']

admin.site.register(CourseEmployee, CourseEmployeeAdmin)