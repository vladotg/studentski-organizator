# -*- coding: utf-8 -*-

from django import template
from organizer.models import PostComment, File

register = template.Library()

@register.assignment_tag
def get_comments_number(post):
    comments_number = PostComment.objects.filter(post=post).count()
    return comments_number

@register.assignment_tag
def get_post_files_number(post):
    files_number = File.objects.filter(post=post).count()
    return files_number