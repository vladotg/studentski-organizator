from django import forms

from organizer.models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ('student', 'date', 'course')

class SendMailForm(forms.Form):
    subject = forms.CharField(label="Predmet", max_length=100, required=True)
    message = forms.CharField(label="Poruka", widget=forms.Textarea, required=True)