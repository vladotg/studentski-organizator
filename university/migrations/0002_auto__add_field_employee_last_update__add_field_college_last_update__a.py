# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Employee.last_update'
        db.add_column(u'university_employee', 'last_update',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 5, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'College.last_update'
        db.add_column(u'university_college', 'last_update',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 5, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'Course.last_update'
        db.add_column(u'university_course', 'last_update',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 5, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'University.last_update'
        db.add_column(u'university_university', 'last_update',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 5, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'CourseEmployee.last_update'
        db.add_column(u'university_courseemployee', 'last_update',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 5, 21, 0, 0)),
                      keep_default=False)

        # Adding field 'Place.last_update'
        db.add_column(u'university_place', 'last_update',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 5, 21, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Employee.last_update'
        db.delete_column(u'university_employee', 'last_update')

        # Deleting field 'College.last_update'
        db.delete_column(u'university_college', 'last_update')

        # Deleting field 'Course.last_update'
        db.delete_column(u'university_course', 'last_update')

        # Deleting field 'University.last_update'
        db.delete_column(u'university_university', 'last_update')

        # Deleting field 'CourseEmployee.last_update'
        db.delete_column(u'university_courseemployee', 'last_update')

        # Deleting field 'Place.last_update'
        db.delete_column(u'university_place', 'last_update')


    models = {
        u'university.college': {
            'Meta': {'object_name': 'College'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.Place']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.University']"})
        },
        u'university.course': {
            'Meta': {'object_name': 'Course'},
            'college': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.College']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'pass_threshold': ('django.db.models.fields.DecimalField', [], {'default': '50', 'max_digits': '5', 'decimal_places': '2'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'})
        },
        u'university.courseemployee': {
            'Meta': {'object_name': 'CourseEmployee'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.Course']"}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.Employee']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'university.employee': {
            'Meta': {'object_name': 'Employee'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'employee_type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'})
        },
        u'university.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {})
        },
        u'university.university': {
            'Meta': {'object_name': 'University'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['university']