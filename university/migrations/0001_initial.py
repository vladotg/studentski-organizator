# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Place'
        db.create_table(u'university_place', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zip_code', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'university', ['Place'])

        # Adding model 'University'
        db.create_table(u'university_university', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'university', ['University'])

        # Adding model 'College'
        db.create_table(u'university_college', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['university.Place'])),
            ('university', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['university.University'])),
        ))
        db.send_create_signal(u'university', ['College'])

        # Adding model 'Course'
        db.create_table(u'university_course', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255)),
            ('college', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['university.College'])),
            ('pass_threshold', self.gf('django.db.models.fields.DecimalField')(default=50, max_digits=5, decimal_places=2)),
        ))
        db.send_create_signal(u'university', ['Course'])

        # Adding model 'Employee'
        db.create_table(u'university_employee', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255)),
            ('employee_type', self.gf('django.db.models.fields.CharField')(max_length=1, null=True)),
        ))
        db.send_create_signal(u'university', ['Employee'])

        # Adding model 'CourseEmployee'
        db.create_table(u'university_courseemployee', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['university.Course'])),
            ('employee', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['university.Employee'])),
        ))
        db.send_create_signal(u'university', ['CourseEmployee'])


    def backwards(self, orm):
        # Deleting model 'Place'
        db.delete_table(u'university_place')

        # Deleting model 'University'
        db.delete_table(u'university_university')

        # Deleting model 'College'
        db.delete_table(u'university_college')

        # Deleting model 'Course'
        db.delete_table(u'university_course')

        # Deleting model 'Employee'
        db.delete_table(u'university_employee')

        # Deleting model 'CourseEmployee'
        db.delete_table(u'university_courseemployee')


    models = {
        u'university.college': {
            'Meta': {'object_name': 'College'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.Place']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.University']"})
        },
        u'university.course': {
            'Meta': {'object_name': 'Course'},
            'college': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.College']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'pass_threshold': ('django.db.models.fields.DecimalField', [], {'default': '50', 'max_digits': '5', 'decimal_places': '2'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'})
        },
        u'university.courseemployee': {
            'Meta': {'object_name': 'CourseEmployee'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.Course']"}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['university.Employee']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'university.employee': {
            'Meta': {'object_name': 'Employee'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'employee_type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'})
        },
        u'university.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {})
        },
        u'university.university': {
            'Meta': {'object_name': 'University'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['university']