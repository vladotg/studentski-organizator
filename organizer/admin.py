#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin 
from models import *

admin.site.register(Activity)
admin.site.register(ActivityType)

admin.site.register(Note)

class ToDoAdmin(admin.ModelAdmin):
    list_display = ['to_do', 'date', 'student', 'course']
    search_fields = ['student__username', 'to_do', 'course']
    raw_id_fields = ['student']

admin.site.register(ToDo, ToDoAdmin)

admin.site.register(Literature)
admin.site.register(Post)
admin.site.register(PostComment)
admin.site.register(File)