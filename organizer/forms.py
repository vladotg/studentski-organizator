from django import forms
from datetimewidget.widgets import DateTimeWidget

from models import Activity, Note, Post

dateTimeOptions = {
    'format': 'dd.mm.yyyy hh:ii',
    'autoclose': 'true',
    }

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        exclude = ('student', 'achieved_points')
        widgets = {
            'start': DateTimeWidget(options=dateTimeOptions),
            'end': DateTimeWidget(options=dateTimeOptions),
        }

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        exclude = ('student',)

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ('student', 'date', 'course')