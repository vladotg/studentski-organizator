# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django.views.generic import RedirectView, TemplateView

from views import *


urlpatterns = patterns('',  
    url(r'^kalendar-aktivnosti$', CalendarView.as_view(), name='calendar'),
    url(r'^calendar-events$', calendar_events, name="calendar_events"),
    url(r'^aktivnost/(?P<activity_slug>[\w-]+)-(?P<activity_id>\d+)$', ActivityView.as_view(), name="activity"),
    url(r'^nova-aktivnost$', EditActivityView.as_view(), name="new_activity"),
    url(r'^nova-aktivnost/(?P<student_course_id>\d+)$', EditActivityView.as_view(), name="new_activity"),
    url(r'^uredi-aktivnost/(?P<activity_slug>[\w-]+)-(?P<activity_id>\d+)$', EditActivityView.as_view(), name="edit_activity"),
    url(r'^delete-activity$', delete_activity, name="delete_activity"),
    url(r'^update-points$', update_points, name="update_points"),
    url(r'^biljeske$', NotesView.as_view(), name='notes'),
    url(r'^nova-biljeska$', EditNoteView.as_view(), name="new_note"),
    url(r'^uredi-biljesku/(?P<note_slug>[\w-]+)-(?P<note_id>\d+)$$', EditNoteView.as_view(), name="edit_note"),
    url(r'^delete-note$', delete_note, name="delete_note"),
    url(r'^new-todo$', new_todo, name="new_todo"),
    url(r'^edit-todo$', edit_todo, name="edit_todo"),
    url(r'^done-todo$', mark_as_done_todo, name="done_todo"),
    url(r'^rearrange-todo$', rearrange_todo, name="rearrange_todo"),
    url(r'^$', HomePageView.as_view(), name='home'),
)
