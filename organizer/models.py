# -*- coding: utf-8 -*-
from django.db import models

from django.conf import settings
from django.utils.translation import ugettext as _
from user_zone.models import Student, StudentCourse
from django.template.defaultfilters import slugify
from decimal import *
from django.core.urlresolvers import reverse

class ActivityType(models.Model):
    name = models.CharField(verbose_name=u'Naziv', max_length=255)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _(u"Tip aktivnosti")
        verbose_name_plural = _(u"Tipovi aktivnosti")

class Activity(models.Model):
    student = models.ForeignKey(Student, verbose_name=u'Student')
    title = models.CharField(verbose_name=u'Naziv', max_length=255)
    activity_type = models.ForeignKey(ActivityType, verbose_name=u'Tip aktivnosti', null=True, blank=True)
    start = models.DateTimeField(verbose_name=u"Vrijeme početka")
    end = models.DateTimeField(verbose_name=u"Vrijeme završetka")    
    course = models.ForeignKey(StudentCourse, verbose_name=u'Kolegij', null=True, blank=True)
    description = models.TextField(verbose_name=u'Opis', null=True, blank=True)
    max_points = models.DecimalField(verbose_name=_(u"Mogući broj bodova"), default=0, max_digits = 5, decimal_places = 2)
    achieved_points = models.DecimalField(verbose_name=_(u"Ostvareni broj bodova"), default=0, max_digits = 5, decimal_places = 2)

    def __unicode__(self):
        return unicode(self.title)

    class Meta:
        verbose_name = _(u"Aktivnost")
        verbose_name_plural = _(u"Aktivnosti")

    def get_slug(self):
        return slugify(self.title)

    def save(self, *args, **kwargs):
        super(Activity, self).save(*args, **kwargs)
        update_course_points(self.course)

    def get_url(self):
        return reverse('activity', args=[slugify(self.title), self.id])

def update_course_points(course):
    activities = Activity.objects.filter(course=course)
    points = Decimal(0)
    for activity in activities:
        points += activity.achieved_points
    course.achieved_points = points
    course.save()

class Note(models.Model):
    student = models.ForeignKey(Student, verbose_name=u'Student')
    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    date = models.DateTimeField(verbose_name=u"Datum", auto_now=True)
    note = models.TextField(verbose_name=u'Bilješka')
    course = models.ForeignKey(StudentCourse, verbose_name=u'Kolegij', null=True, blank=True)
    position = models.IntegerField(verbose_name=_("Redoslijed"), default=0)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name=_(u"Bilješka")
        verbose_name_plural=_(u"Bilješke")
        ordering=('-date',)

    def get_slug(self):
        return slugify(self.name)

class ToDo(models.Model):
    student = models.ForeignKey(Student, verbose_name=u'Student')
    date = models.DateTimeField(verbose_name=u"Datum", auto_now=True)
    to_do = models.CharField(verbose_name=u'To do', max_length=255)
    course = models.ForeignKey(StudentCourse, verbose_name=u'Kolegij', null=True, blank=True)
    position = models.IntegerField(verbose_name=_("Redoslijed"), default=0)
    done = models.BooleanField(verbose_name=u'Obavljeno', default=False)

    def __unicode__(self):
        return unicode(self.to_do)

    class Meta:
        verbose_name=_(u"TO DO")
        verbose_name_plural=_(u"TO DO")
        ordering=('position',)

class Literature(models.Model):
    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    student = models.ForeignKey(Student, verbose_name=u'Student')
    course = models.ForeignKey(StudentCourse, verbose_name=u'Kolegij')
    authors = models.TextField(verbose_name=u'Autori')
    description = models.TextField(verbose_name=u'Opis', null=True, blank=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name=_(u"Literatura")
        verbose_name_plural=_(u"Literatura")

class Post(models.Model):
    date = models.DateTimeField(verbose_name=u"Datum", auto_now=True)
    text = models.TextField(verbose_name=u'Tekst')
    student = models.ForeignKey(Student, verbose_name=u'Student')
    course = models.ForeignKey(StudentCourse, verbose_name=u'Kolegij')

    def __unicode__(self):
        return unicode(self.text)

    class Meta:
        verbose_name=_(u"Objava")
        verbose_name_plural=_(u"Objave")
        ordering=('-date',)

class PostComment(models.Model):
    date = models.DateTimeField(verbose_name=u"Datum", auto_now=True)
    text = models.TextField(verbose_name=u'Tekst')
    student = models.ForeignKey(Student, verbose_name=u'Student')
    post = models.ForeignKey(Post, verbose_name=u'Objava')

    def __unicode__(self):
        return unicode(self.text)

    class Meta:
        verbose_name=_(u"Komentar na objavu")
        verbose_name_plural=_(u"Komentari na objavu")
        ordering=('date',)

class File(models.Model):
    root_folder = 'files'

    name = models.CharField(verbose_name=u'Naziv', max_length=255)
    date = models.DateTimeField(verbose_name=u"Datum")
    description = models.TextField(verbose_name=u'Tekst', null=True, blank=True)
    student = models.ForeignKey(Student, verbose_name=u'Student')
    post = models.ForeignKey(Post, verbose_name=u'Objava', null=True, blank=True)
    position = models.IntegerField(verbose_name=_("Redoslijed"), default=0)
    filename = models.FileField(verbose_name=_(u"Datoteka"), upload_to='student_files', max_length=255)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name=_(u"Datoteka")
        verbose_name_plural=_(u"Datoteke")
        ordering=('position',)