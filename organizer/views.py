# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.utils import simplejson
from datetime import datetime
from forms import ActivityForm, NoteForm
from user_zone.models import StudentCourse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from decimal import *

from .models import (
        Activity,
        Note,
        ToDo, 
        Post
    )

class HomePageView(TemplateView):
    template_name = "organizer/page_home.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HomePageView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)

        student_courses = StudentCourse.objects.filter(student=self.request.user)
        upcoming_activities = Activity.objects.filter(start__gte=datetime.now(), student=self.request.user)\
            .order_by('start')[:4]
        latest_posts = Post.objects.all().order_by('-date')[:5]

        context['pagetitle'] = 'Naslovnica'
        context['student_courses'] = student_courses
        context['upcoming_activities'] = upcoming_activities
        context['latest_posts'] = latest_posts

        return context

class CalendarView(TemplateView):
    template_name = "organizer/calendar.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CalendarView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CalendarView, self).get_context_data(**kwargs)

        student_courses = StudentCourse.objects.filter(student=self.request.user)

        context['top_nav'] = [{'label':'Nova aktivnost', 'url':reverse('new_activity'), 'icon':'icon-plus'}]        
        context['pagetitle'] = 'Kalendar'
        context['student_courses'] = student_courses

        return context

class ActivityView(TemplateView):
    template_name = "organizer/activity.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ActivityView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ActivityView, self).get_context_data(**kwargs)

        activity_id = kwargs.get('activity_id')

        activity = get_object_or_404(Activity, id = activity_id)

        context['top_nav'] = [
            {'label':'Uredi', 'url':reverse('edit_activity', args=[activity.get_slug(), activity.id]), 'icon':'icon-edit'},
            
        ]
        context['activity'] = activity
        context['pagetitle'] = 'Kalendar'

        return context

class EditActivityView(TemplateView):
    template_name = "organizer/edit_activity.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        activity_id = kwargs.get('activity_id', None)
        course_id = kwargs.get('student_course_id', None)
        try:
            self.activity = Activity.objects.get(id=activity_id)
        except Activity.DoesNotExist:
            self.activity = Activity(student=self.request.user)
        try:
            student_course = StudentCourse.objects.get(id=course_id)
            self.activity.course = student_course
        except StudentCourse.DoesNotExist:
            pass
        return super(EditActivityView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditActivityView, self).get_context_data(**kwargs)

        form = ActivityForm(instance=self.activity) 
        form.fields["course"].queryset = StudentCourse.objects.filter(student = self.request.user)

        context['form'] = form
        context['pagetitle'] = 'Kalendar'

        return context

    def post(self, *args, **kwargs):
        form = ActivityForm(self.request.POST, instance=self.activity)

        if form.is_valid():
            activity = form.save(commit=False)
            activity.save()

            messages.success(self.request, u"Uspješno ste spremili aktivnost!")
            return HttpResponseRedirect(reverse('calendar'))

        form.fields["course"].queryset = StudentCourse.objects.filter(student = self.request.user)
        return self.render_to_response({'form': form, 'pagetitle':'Kalendar'})


def calendar_events(request):
    start = datetime.fromtimestamp(int(request.GET.get('start')))
    end = datetime.fromtimestamp(int(request.GET.get('end')))

    activities = Activity.objects.filter(student = request.user, start__gte=start, end__lte=end)

    events_array = []

    for activity in activities:
        events_array += {
            'id' : activity.id,
            'title' : activity.title,
            'start' : activity.start.strftime('%Y-%m-%dT%H:%M:%S') ,
            'end' : activity.end.strftime('%Y-%m-%dT%H:%M:%S') ,
            'url' : reverse('activity', args=[activity.get_slug(), activity.id]),
            'color' : activity.course.color if activity.course else '',
            },

    events = simplejson.dumps(events_array)

    return HttpResponse(events, mimetype='application/json')

class NotesView(TemplateView):
    template_name = "organizer/notes.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NotesView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NotesView, self).get_context_data(**kwargs)

        notes_list = Note.objects.filter(student=self.request.user)

        paginator = Paginator(notes_list, 4)
    
        page = self.request.GET.get('page')
        
        try:
            notes = paginator.page(page)
        except PageNotAnInteger:
            notes = paginator.page(1)
        except EmptyPage:
            notes = paginator.page(paginator.num_pages)

        todos = ToDo.objects.filter(student=self.request.user, done=False)

        context['top_nav'] = [{'label':'Nova bilješka', 'url':reverse('new_note'), 'icon':'icon-plus'}]
        context['pagetitle'] = u'Bilješke'
        context['notes'] = notes
        context['todos'] = todos

        return context

class EditNoteView(TemplateView):
    template_name = "organizer/edit_note.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        note_id = kwargs.get('note_id', None)
        try:
            self.note = Note.objects.get(id=note_id)
        except Note.DoesNotExist:
            self.note = Note(student=self.request.user)
        return super(EditNoteView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditNoteView, self).get_context_data(**kwargs)

        form = NoteForm(instance=self.note) 
        form.fields["course"].queryset = StudentCourse.objects.filter(student = self.request.user)

        context['form'] = form
        context['pagetitle'] = u'Bilješke'

        return context

    def post(self, *args, **kwargs):
        form = NoteForm(self.request.POST, instance=self.note)

        if form.is_valid():
            note = form.save(commit=False)
            note.save()

            messages.success(self.request, u"Uspješno ste spremili bilješku!")
            return HttpResponseRedirect(reverse('notes'))

        form.fields["course"].queryset = StudentCourse.objects.filter(student = self.request.user)
        return self.render_to_response({'form': form})

def new_todo(request):
    todo_str = request.POST.get('todo_str', 'Nova stavka')
    todo = ToDo(student=request.user, to_do=todo_str)
    todo.save()

    return render(request, 'organizer/single_todo.html', {"todo": todo})

def edit_todo(request):
    todo_id = int(request.POST.get('todo_id', None))
    todo_str = request.POST.get('todo_str', None)
    try:
        todo = ToDo.objects.get(id=todo_id)
        todo.to_do = todo_str
        todo.save()
        return HttpResponse('OK', status=200)
    except ToDo.DoesNotExist:
        return HttpResponse("Exception", status=404)

def mark_as_done_todo(request):
    todo_id = int(request.POST.get('todo_id', None))
    try:
        todo = ToDo.objects.get(id=todo_id)
        todo.done=True
        todo.save()
        return HttpResponse('OK', status=200)
    except ToDo.DoesNotExist:
        return HttpResponse("Exception", status=404)

def rearrange_todo(request):
    positions = request.POST.getlist('positions[]')

    order=1
    string = ''
    for position in positions:
        string += 'WHEN %d THEN %d ' % (int(position), order)
        order+=1

    sql = 'UPDATE organizer_todo SET position = CASE id %s ELSE position END;' % string

    from django.db import connection, transaction
    cursor = connection.cursor()
    cursor.execute(sql)
    transaction.commit_unless_managed()

    return HttpResponse('OK', status=200)

def delete_note(request):
    note_id = int(request.POST.get('note_id', None))
    try:
        note = Note.objects.get(id=note_id)
        note.delete()
        return HttpResponse('OK', status=200)
    except Note.DoesNotExist:
        return HttpResponse("Exception", status=404)

def delete_activity(request):
    activity_id = int(request.POST.get('activity_id', None))
    try:
        activity = Activity.objects.get(id=activity_id)
        activity.delete()
        return HttpResponse('OK', status=200)
    except Activity.DoesNotExist:
        return HttpResponse("Exception", status=404)

def update_points(request):
    activity_id = int(request.POST.get('activity_id', None))
    points = Decimal(request.POST.get('points', None))
    try:
        activity = Activity.objects.get(id=activity_id)
        activity.achieved_points = points
        activity.save()
        return HttpResponse('OK', status=200)
    except Activity.DoesNotExist:
        return HttpResponse("Exception", status=404)