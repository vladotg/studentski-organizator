# -*- coding: utf-8 -*-

from django import template
from user_zone.models import StudentCourse

register = template.Library()

@register.assignment_tag
def get_student_course(student):
    try:
        student_course = StudentCourse.objects.filter(student=student)[0]
        return student_course
    except KeyError:
        return None