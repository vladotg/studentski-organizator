# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from models import *

class StudentCourseInline(admin.TabularInline):
    model = StudentCourse

class StudentAdmin(UserAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    list_display = ['username', 'first_name', 'last_name', 'email', 'college', 'last_update']
    list_filter = ['college', 'gender']
    search_fields = ['first_name', 'last_name']
    #exclude = ['last_update']
    inlines = [StudentCourseInline]

admin.site.register(Student, StudentAdmin)
