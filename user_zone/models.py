# -*- coding: utf-8 -*-
from django.db import models

from django.contrib.auth.models import User, AbstractUser
from django.utils.translation import ugettext as _

from django.conf import settings
from university.models import College, Course
from decimal import *

USER_GENDER = (
    ('M', _(u"Muški")),
    ('F', _(u"Ženski")),    
)

class Student(AbstractUser):
    gender = models.CharField(verbose_name=_(u"Spol"), choices=USER_GENDER, max_length=1, null=True, blank=True)
    date_of_birth = models.DateField(verbose_name=_(u"Datum rođenja"), null=True, blank=True)
    college = models.ForeignKey(College, verbose_name=u'Fakultet', null=True, blank=True)
    join_date = models.DateField(verbose_name=_(u"Datum prvog upisa"), null=True, blank=True)
    current_year = models.IntegerField(verbose_name=_(u"Trenutna godina"), default=1)
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena", null=True, blank=True)

    def __unicode__(self):
        return unicode(self.username)

    class Meta:
        verbose_name = _(u"Student")
        verbose_name_plural = _(u"Studenti")

class StudentCourse(models.Model):
    student = models.ForeignKey(Student, verbose_name=u'Student')
    course = models.ForeignKey(Course, verbose_name=u'Kolegij')
    last_update = models.DateTimeField(verbose_name=u"Zadnja promjena")
    color = models.CharField(verbose_name=u'Boja za prikaz', default='#3a87ad', max_length=32)
    achieved_points = models.DecimalField(verbose_name=_(u"Ostvareni broj bodova"), default=0, max_digits = 5, decimal_places = 2)

    def __unicode__(self):
        return unicode(self.course.name)

    class Meta:
        verbose_name = _(u"Kolegij studenta")
        verbose_name_plural = _(u"Kolegiji studenta")
        ordering = ('course__name',)