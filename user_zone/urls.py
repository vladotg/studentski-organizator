# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django.views.generic import RedirectView, TemplateView

from views import *


urlpatterns = patterns('',  
    #url(r'^moji-kolegiji/(?P<course_slug>[\w-]+)-(?P<course_id>\d+)$', UserCourseView.as_view(), name='user_course'),
    url(r'^moji-kolegiji', UserCoursesView.as_view(), name='user_courses'),
    url(r'^update-color$', update_color, name="update_color"),
    url(r'^prijava', log_in, name='login'),
    url(r'^odjava$', log_out, name="logout"),
)
