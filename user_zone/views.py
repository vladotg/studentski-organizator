# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.utils import simplejson
from datetime import datetime, timedelta
from user_zone.models import StudentCourse
from django.contrib import messages
from forms import AuthenticationForm
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout

from models import *

class UserCoursesView(TemplateView):
    template_name = "user_zone/user_course.html"

    def get_context_data(self, **kwargs):
        context = super(UserCoursesView, self).get_context_data(**kwargs)

        
        context['pagetitle'] = 'Moji kolegiji'

        return context

def log_in(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))

    if request.method == 'POST': 
        form = AuthenticationForm(request.POST) 
        if form.is_valid():
            login(request, form.user_cache)
            if form.cleaned_data['remember_me']:
                request.session.set_expiry(timedelta(days=400))
            else:
                request.session.set_expiry(0)
            next = request.GET.get('next', reverse('home'))
            print "authenticated"
            return HttpResponseRedirect(next)
    else:
        form = AuthenticationForm() 

    data = {
        'form': form,
        'query_string': request.META.get('QUERY_STRING', ''),
    }

    return render(request, 'admin/login.html', data)

def log_out(request):
    logout(request)
    next = request.GET.get('next', reverse('home'))
    return HttpResponseRedirect(next)

def update_color(request):
    student_course_id = int(request.POST.get('student_course_id', None))
    color = request.POST.get('color', None)
    try:
        student_course = StudentCourse.objects.get(id=student_course_id)
        student_course.color = color
        student_course.save()
        return HttpResponse('OK', status=200)
    except StudentCourse.DoesNotExist:
        return HttpResponse("Exception", status=404)