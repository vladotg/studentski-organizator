# -*- coding: utf-8 -*-
from django import forms
from django.forms.widgets import RadioSelect, PasswordInput, Select, Textarea, CheckboxSelectMultiple
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from datetime import datetime, date
from django.utils.translation import ugettext as _
from models import *

class AuthenticationForm(forms.Form):
    username = forms.CharField(label=u"Korisničko ime", max_length=30, required=True)
    password = forms.CharField(label=u"Lozinka", widget=PasswordInput, required=True)
    remember_me = forms.BooleanField(label="Zapamti me", required=False)

    def clean(self):
        cd = self.cleaned_data
        username = cd.get('username')
        password = cd.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if not self.user_cache:
                msg = u"Upisani e-mail ili lozinka su netočni."
                self._errors['username'] = self.error_class([msg])
                return cd
            if self.user_cache and not self.user_cache.is_active:
                msg = u"Vaš korisnički račun nije aktivan."
                self._errors['username'] = self.error_class([msg])
                return cd

        return cd